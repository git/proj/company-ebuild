;;; company-ebuild-custom.el --- Company-Ebuild customization -*- lexical-binding: t -*-



;; Copyright 2022 Gentoo Authors


;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.



;;; Commentary:


;; Company-Ebuild customization.



;;; Code:


(defgroup company-ebuild nil
  "Company backend for editing Ebuild files."
  :group 'company
  :group 'ebuild)


(defcustom company-ebuild-qsearch-executable (executable-find "qsearch")
  "Path to the \"qsearch\" executable binary."
  :safe 'stringp
  :type 'file
  :group 'company-ebuild)

(defcustom company-ebuild--regenerate-dynamic-keywords-eclass t
  "Whether to regenerate ‘company-ebuild--dynamic-keywords-eclass’."
  :type 'boolean
  :group 'company-ebuild)

(defcustom company-ebuild--regenerate-dynamic-keywords-use-flags t
  "Whether to regenerate ‘company-ebuild--dynamic-keywords-use-flags’."
  :type 'boolean
  :group 'company-ebuild)

(defcustom company-ebuild--regenerate-dynamic-keywords-packages t
  "Whether to regenerate ‘company-ebuild--dynamic-keywords-packages’."
  :type 'boolean
  :group 'company-ebuild)

(defcustom company-ebuild--regenerate-dynamic-keywords-licenses t
  "Whether to regenerate ‘company-ebuild--dynamic-keywords-licenses’."
  :type 'boolean
  :group 'company-ebuild)


(provide 'company-ebuild-custom)



;;; company-ebuild-custom.el ends here
