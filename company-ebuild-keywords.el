;;; company-ebuild-keywords.el --- Company-Ebuild keywords -*- lexical-binding: t -*-



;; Copyright 2022 Gentoo Authors


;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.



;;; Commentary:


;; Company-Ebuild keywords.  Boilerplate.  ;^)



;;; Code:


(require 'ebuild-mode)
(require 'ebuild-mode-keywords)


;; Keywords from ebuild-mode font lock.

(defconst company-ebuild--constant-keywords-architectures
  (append '("-*")
          (mapcar (lambda (s) (concat "-" s)) ebuild-mode-arch-list)
          (mapcar (lambda (s) (concat "~" s)) ebuild-mode-arch-list)
          ebuild-mode-arch-list))

(defconst company-ebuild--constant-keywords-restrict
  ebuild-mode-restrict-list)

(defconst company-ebuild--constant-keywords-phases
  (car ebuild-mode-keywords-functions))

(defconst company-ebuild--constant-keywords-sandbox
  (car ebuild-mode-keywords-sandbox))

(defconst company-ebuild--constant-keywords-eclassdoc
  (append (car ebuild-mode-keywords-eclassdoc)
          (car ebuild-mode-keywords-eclassdoc-warn)))

(defconst company-ebuild--constant-keywords-functions
  (append (car ebuild-mode-keywords-eclass)
          (car ebuild-mode-keywords-0)))

;; See: https://devmanual.gentoo.org/ebuild-writing/variables/

(defconst company-ebuild--constant-keywords-variables-predefined
  '("P"
    "PN"
    "PV"
    "PR"
    "PVR"
    "PF"
    "A"
    "CATEGORY"
    "FILESDIR"
    "WORKDIR"
    "T"
    "D"
    "HOME"
    "ROOT"
    "DISTDIR"
    "EPREFIX"
    "ED"
    "EROOT"
    "SYSROOT"
    "ESYSROOT"
    "BROOT"
    "MERGE_TYPE"
    "REPLACING_VERSIONS"
    "REPLACED_BY_VERSION"))

(defconst company-ebuild--constant-keywords-variables-ebuild-defined
  '("EAPI"
    "DESCRIPTION"
    "HOMEPAGE"
    "SRC_URI"
    "LICENSE"
    "SLOT"
    "KEYWORDS"
    "IUSE"
    "REQUIRED_USE"
    "PROPERTIES"
    "RESTRICT"
    "S"
    "DOCS"
    "HTML_DOCS"))

(defconst company-ebuild--constant-keywords-variables-dependencies
  '("DEPEND"
    "BDEPEND"
    "RDEPEND"
    "IDEPEND"
    "PDEPEND"))

(defconst company-ebuild--constant-keywords-variables-user-environment
  '("AR"
    "ARFLAGS"
    "AS"
    "ASFLAGS"
    "CC"
    "CFLAGS"
    "CPPFLAGS"
    "CXX"
    "CXXFLAGS"
    "LD"
    "LDFLAGS"
    "LEX"
    "LFLAGS"
    "NM"
    "RANLIB"
    "YACC"
    "YFLAGS"))

(defconst company-ebuild--constant-keywords
  (append company-ebuild--constant-keywords-architectures
          company-ebuild--constant-keywords-restrict
          company-ebuild--constant-keywords-phases
          company-ebuild--constant-keywords-sandbox
          company-ebuild--constant-keywords-eclassdoc
          company-ebuild--constant-keywords-functions
          company-ebuild--constant-keywords-variables-predefined
          company-ebuild--constant-keywords-variables-ebuild-defined
          company-ebuild--constant-keywords-variables-dependencies
          company-ebuild--constant-keywords-variables-user-environment))


;; Dynamically collected keywords.

(defvar company-ebuild--dynamic-keywords-eclasses nil)

(defvar company-ebuild--dynamic-keywords-functions nil)

(defvar company-ebuild--dynamic-keywords-licenses nil)

(defvar company-ebuild--dynamic-keywords-packages nil)

(defvar company-ebuild--dynamic-keywords-use-flags nil)

(defvar company-ebuild--dynamic-keywords-variables nil)


(defun company-ebuild--dynamic-keywords ()
  "Return a list of all dynamic Ebuild keywords."
  (append company-ebuild--dynamic-keywords-eclasses
          company-ebuild--dynamic-keywords-functions
          company-ebuild--dynamic-keywords-variables
          company-ebuild--dynamic-keywords-use-flags
          company-ebuild--dynamic-keywords-packages
          company-ebuild--dynamic-keywords-licenses))

(defun company-ebuild--executables (arg)
  "Return a list of available executables matching ARG on current system."
  (locate-file-completion-table exec-path exec-suffixes arg 'identity t))


(provide 'company-ebuild-keywords)



;;; company-ebuild-keywords.el ends here
